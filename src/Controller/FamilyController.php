<?php

namespace App\Controller;

use App\Entity\Family;
use App\Form\FamilyType;
use App\Repository\FamilyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/families", name="family_", requirements={"id"="[0-9a-f-]+"})
 */
class FamilyController extends AbstractController
{

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function family_index(FamilyRepository $familyRepository): Response
    {

        $families = $familyRepository->findAllOrderedByName();

        return $this->render('family/index.html.twig', [
            'families' => $families
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function family_new(Request $request, EntityManagerInterface $manager): Response
    {
        $family = new Family();
        $creationForm = $this->createForm(FamilyType::class, $family, [
            'askForTerms' => true
        ]);

        $creationForm->handleRequest($request);
        if($creationForm->isSubmitted() && $creationForm->isValid()){
            $manager->persist($family);
            $manager->flush();

            return $this->redirectToRoute("family_index");
        }

        return $this->render('family/new.html.twig', [
            "creationForm" => $creationForm->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function family_show(Family $family): Response
    {
        return $this->render('family/show.html.twig', [
            'family' => $family
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function family_edit($id, FamilyRepository $familyRepository, Request $request, EntityManagerInterface $manager): Response
    {

        // $family = $familyRepository->find($id); // Automatiquement chercher par id
        // $family = $familyRepository->findOneBy(['id' => $id]); // Précision manuelle de quels champs doivent matcher
        $family = $familyRepository->findOneById($id); // FindOneByNomDuCham

        $editionForm = $this->createForm(FamilyType::class, $family);
        $editionForm->handleRequest($request);
        if($editionForm->isSubmitted() && $editionForm->isValid()){
            $manager->flush();

            return $this->redirectToRoute('family_index');
        }

        return $this->render('family/edit.html.twig', [
            'family' => $family,
            'editionForm' => $editionForm->createView()
        ]);
    }

}
