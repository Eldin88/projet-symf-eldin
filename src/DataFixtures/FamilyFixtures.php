<?php

namespace App\DataFixtures;

use App\Entity\Family;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FamilyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $names = [
          "Ducommun",
          "Duvoisin",
          "Miller",
          "Dupond",
          "Moore",
          "Lemarchal",
          "Hendrix"
        ];

        foreach ($names as $name){
            $family = new Family();
            $family->setName($name);
            $manager->persist($family);
        }

        $manager->flush();
    }
}
